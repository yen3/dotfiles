# zM: foldall
# zinit source {{{
# Ref: https://gist.github.com/numToStr/53a0bf7e8ac9c5aa98d52c112980fb0f
ZINIT_DIR="$HOME/.local/share/zinit"
ZINIT_HOME="$ZINIT_DIR/zinit.git"
if [[ ! -f $ZINIT_HOME/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$ZINIT_DIR" && command chmod g-rwX "$ZINIT_DIR"
    command git clone https://github.com/zdharma-continuum/zinit "$ZINIT_HOME" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$ZINIT_HOME/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit
# }}}
# prompt setting {{{
export SPACESHIP_CONFIG="${HOME}/usr/rc/zsh/spaceship.zsh"
zinit light yen3/spaceship-prompt
# }}}
# oh my zsh {{{
# IMPORTANT:
# Ohmyzsh plugins and libs are loaded first as some these sets some defaults which are required later on.
# Otherwise something will look messed up
# ie. some settings help zsh-autosuggestions to clear after tab completion

setopt promptsubst

zinit wait lucid for \
	OMZL::compfix.zsh \
	OMZL::correction.zsh \
    atload'
        function take() {
            mkdir -p $@ && cd ${@:$#}
        }
        alias rm="rm -rf"
    ' \
	OMZL::git.zsh \
	OMZL::grep.zsh \
	OMZL::spectrum.zsh \
	OMZL::termsupport.zsh \
	# OMZP::docker-compose \
	# as"completion" OMZP::docker/_docker
    # djui/alias-tips

# }}}
# Autocomplete and suggesstion {{{

zinit wait lucid for \
    light-mode blockf atpull'zinit creinstall -q .' \
    atinit"
        zstyle ':completion:*' completer _expand _complete _ignored _approximate
        zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
        zstyle ':completion:*' menu select=2
        zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
        zstyle ':completion:*:descriptions' format '-- %d --'
        zstyle ':completion:*:processes' command 'ps -au$USER'
        zstyle ':completion:complete:*:options' sort false
        zstyle ':fzf-tab:complete:_zlua:*' query-string input
        zstyle ':completion:*:*:*:*:processes' command 'ps -u $USER -o pid,user,comm,cmd -w -w'
        zstyle ':fzf-tab:complete:kill:argument-rest' extra-opts --preview=$extract'ps --pid=$in[(w)1] -o cmd --no-headers -w -w' --preview-window=down:3:wrap
        zstyle ':fzf-tab:complete:cd:*' extra-opts --preview=$extract'exa -1 --color=always ${~ctxt[hpre]}$in'
    " \
        zsh-users/zsh-completions \
    light-mode atinit"
        ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20;
        ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=240';
        bindkey '^e' autosuggest-accept;
    " atload"_zsh_autosuggest_start" \
        zsh-users/zsh-autosuggestions \
    light-mode atinit"
        typeset -gA FAST_HIGHLIGHT;
        FAST_HIGHLIGHT[git-cmsg-len]=100;
        zpcompinit;
        zpcdreplay;
    " \
        zdharma-continuum/fast-syntax-highlighting
# }}}
# prezto {{{
zinit snippet PZT::modules/helper/init.zsh
zinit snippet PZT::modules/environment/init.zsh
zinit snippet PZT::modules/docker/alias.zsh
zinit snippet PZT::modules/command-not-found/init.zsh
zinit snippet PZT::modules/directory/init.zsh

# zinit snippet PZT::modules/tmux/init.zsh
zinit snippet PZT::modules/history/init.zsh

zinit ice svn wait=1 as=null lucid
zinit snippet PZTM::archive
# }}}
# shell functions {{{
# zinit load agkozak/zsh-z

zt(){ zinit depth'3' lucid ${1/#[0-9][a-c]/wait"${1}"} "${@:2}"; }
zt 0b light-mode for \
    autoload'#manydots-magic' \
        knu/zsh-manydots-magic

zt light-mode for \
    trigger-load'!man' \
        ael-code/zsh-colored-man-pages

zinit ice wait'1' lucid
zinit light hlissner/zsh-autopair

zinit light paulirish/git-open
zinit light Tarrasch/zsh-bd

# lfcd
zinit snippet https://raw.githubusercontent.com/gokcehan/lf/master/etc/lfcd.sh

export FORGIT_NO_ALIASES=1
zinit light wfxr/forgit

# zi ice as"program" pick"$ZPFX/bin/git-*" make"PREFIX=$ZPFX"
# zi light tj/git-extras
# }}}
# Setopt: {{{
setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_all_dups   # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt inc_append_history     # add commands to HISTFILE in order of execution
setopt share_history          # share command history data
setopt always_to_end          # cursor moved to the end in full completion
setopt hash_list_all          # hash everything before completion
setopt completealiases        # complete alisases
setopt always_to_end          # when completing from the middle of a word, move the cursor to the end of the word
setopt complete_in_word       # allow completion from within a word/phrase
setopt nocorrect              # spelling correction for commands
setopt list_ambiguous         # complete as much of a completion until it gets ambiguous.
setopt nolisttypes
setopt listpacked
setopt automenu

ZSH_AUTOSUGGEST_MANUAL_REBIND=1  # make prompt faster
DISABLE_MAGIC_FUNCTIONS=true     # make pasting into terminal faster
# }}}

# vim: ft=zsh foldmethod=marker foldcolumn=1 foldlevelstart=0
