return {
  { "nvim-lua/plenary.nvim", event = "VeryLazy" },

  -- Misc
  { "dstein64/vim-startuptime", cmd = "StartupTime" },
}
