return {
  colors = {
    foreground = "#abb2bf",
    background = "#272b33",
    selection_bg = "#555555",
    selection_fg= "#cfd2d0",
    cursor_bg = "#d8d8d8",
    cursor_fg = "#1b1b1b",
    ansi = {
      "#272b33",  -- black
      "#ca8288",  -- red
      "#81ad61",  -- green
      "#dcc342",  -- yellow
      "#528bff",  -- blue
      "#6972f3",  -- orange (magentas usually)
      "#56b6c2",  -- cyan
      "#abb2bf",  -- white
    },

    brights = {
      "#31353f",  -- black
      "#f97782",  -- red
      "#aadb87",  -- green
      "#ffd589",  -- yellow
      "#68bbfe",  -- blue
      "#dc85f7",  -- orange (magentas usually)
      "#60ccdb",  -- cyan
      "#bac2d4",  -- white
    },
    tab_bar = {
      background = "#263038",
      new_tab = {bg_color = "#263038", fg_color = "#abb2bf", intensity = "Bold"},
      new_tab_hover = {bg_color = "#263038", fg_color = "#abb2bf", intensity = "Bold"},
      active_tab = {bg_color = "#394351", fg_color = "#abb2bf"},
      inactive_tab = { bg_color = '#263038', fg_color = '#abb2bf', },
    }
  }
}
